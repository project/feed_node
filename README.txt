
README
==========================================
The purpose of this module is to provide a simple method 
for displaying aggregator feed items within a node framework.  
One can "feed enable" a particular node type which allows the 
user to attach a single feed to a node by specifying a feed title and URL.  

An alternative module which performs a similar function and integrates
with CCK is the feedfield module: http://drupal.org/project/feedfield

This module provides two display styles (title, and title + teaser) and 
allows the user to configure the maximum number of feed items to
display.  As well, the module ensures that all aspects of an aggregator feed
and it's corresponding feed items are available in the node data
structure so that custom themeing can be achieved if the default
themes are not adequate.

The updating of an individual aggregator feed's items is performed
by the aggregator.  Once a new feed is added, it's items will not be
displayed until the user has manually updated the feed, or the aggregator
has updated the feed via the cron hook.  A future enhancement would be
to perform this automatically or provide the node author a link to
update the associated feed items.

If the title and URL of the feed correspond to an existing feed already
in the aggregator table, then that feed will be used (no duplicates will 
be created).  If the title OR URL are changed, then the title OR URL of
the corresponding feed will be changed in aggregator.  

AUGMENTED NODE STRUCTURE
==========================================
$node->feed_node
$node->feed_node['fid']
$node->feed_node['nid']
$node->feed_node['numItems']
$node->feed_node['displayStyle']
$node->feed_node['aggregator_feed']  - an array containing aggregator_feed database row values
$node->feed_node['aggregator_items'] - an array of arrays containing aggregator_item database row values

Author
==========================================
David Ayre <david at bryght dot com>
Sponsored by Bryght <http://www.bryght.com>