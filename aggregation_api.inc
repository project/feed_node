<?php
/**
 * Aggregation API
 * 
 * Drupal is a great aggregation platform. This is an attempt to 
 * create an API to allow unified access to shared aggregator 
 * functions - adding, updating and removing feeds, and querying 
 * existing feeds. 
 * 
 * Currently, this same file is also used in the feedfield project.
 * Once we achieve a more stable and functional API, online one
 * version will be used, and hopefully incorporated into other
 * aggregation related modules.
 * 
 */

/**
 * Phase 1: Create a set of functions to work with Aggregator. 
 * Phase 2: Create an extensible system of hooks to allow other modules to implement the API
 */

/**
 * Returns a valid feed ID for a feed which has the specified url and/or title. 
 * This function first checks to see if a feed with the given URL exists in the 
 * aggregator table.  If so, then an update is made to ensure that feed has the 
 * same title as the one specified (syncing on URL).  If no feed exists with 
 * the given URL, then a feed search is done by title.  If found, then the
 * feed is updated to ensure it has the same url as the one specified (sync on title).
 * If no feed exists with either the specified url or title, then a new aggregator
 * feed entry is made with the supplied values, and the new aggregator feed ID
 * is returned.
 * 
 * The purpose of this method is provide a quick method of editing the aggregator
 * feed information from within the feed node itself (a quick title or URL change,
 * or a complete new feed addition) so the user doesn't have to go to the aggregator
 * admin section.  
 * 
 * @param $title
 * @param $url
 * @return
 * 	 Returns the id for the corresponding aggregator feed in the database or
 *   -1 if a conflict exists with an existing feed in the database.
 */
function aggregation_sync_feed($title, $url) {
  
  // try to find a feed with the given URL
  $urlfeed   = aggregation_get_feed_by_url($url);
  $titlefeed = aggregation_get_feed_by_title($title);
  
  // if there is no feed with the given url or title, we have an insert
  if (!$urlfeed && !$titlefeed) {
    $newfeed['url']=$url;
 	  $newfeed['title']=$title;
 	  return _aggregation_insert_feed($newfeed);
  }
  // if a feed exists with the URL, but none with the given title,
  // we update the title
  else if ($urlfeed && !$titlefeed) {
    $urlfeed['title'] = $title;
    return _aggregation_update_feed($urlfeed);
  }
  // if a feed exists with the title, but none with the given url,
  // we update the url
  else if (!$urlfeed && $titlefeed) {
    $titlefeed['url'] = $url;
    return _aggregation_update_feed($titlefeed);
  }
  
	// if feeds exists with the given url and title and they are the same,
	// then there is nothing to update
	if ($urlfeed['fid'] == $titlefeed['fid']) {
	  return $urlfeed['fid'];
	}
   
	// else a feed exists with the given URL, and a different feed exists with
	// the given title, so we have a conflict the user must resolve
  return -1;

}


/**
 * Removes an aggregator feed and all associated aggregator feed items from
 * the database.  This function also removes pointers from the aggregator
 * category tables to the feed and feed items.
 * 
 * @param $fid
 */
function aggregation_delete_feed($fid) {
  if ($fid) {
    db_query('DELETE FROM {aggregator_category_feed} WHERE fid = %d', $fid);
    $result = db_query('SELECT iid FROM {aggregator_item} WHERE fid = %d', $fid);
    while ($item = db_fetch_object($result)) {
      $items[] = "iid = $item->iid";
    }
    if ($items) {
      db_query('DELETE FROM {aggregator_category_item} WHERE '. implode(' OR ', $items));
    }
    db_query('DELETE FROM {aggregator_feed} WHERE fid = %d', $fid);
    db_query('DELETE FROM {aggregator_item} WHERE fid = %d', $fid);
  }
}

/**
 * Remove all the items that belong to a feed
 *
 * @param $fid
 */
function aggregation_remove_feed_items($fid){
  aggregator_remove($fid);
}

//---------------- General feed identification functions
/**
 * Load a feed based on the url of the feed
 *
 * @param $url The feed url
 * @return 
 *   A feed array
 */
 function aggregation_get_feed_by_url($url) {
   if (!$url) return FALSE;
   return db_fetch_array(db_query("SELECT * FROM {aggregator_feed} WHERE url = '%s'", $url));
 }

/**
 * Load a feed based on it's id
 *
 * @param $fid Id of feed to load
 * @return
 *   An array of feed data
 */
 function aggregation_get_feed_by_id($fid) {
   return aggregator_get_feed($fid);
 }


/**
 * Returns a feed array (the first) for a feed with the specified title
 * 
 * @param $title
 * @return
 * 	 A array of aggregator feed information.
 */
 function aggregation_get_feed_by_title($title) {
   if (!$title) return FALSE;
   return db_fetch_array(db_query("SELECT * FROM {aggregator_feed} WHERE title = '%s'", $title));
 }




 //------------------- Aggregator category functions

/**
 * Assign categories to a specific feed 
 *
 * @param $fid Feed Id 
 * @param $categories An array of categories to assign  
 */
 function aggregation_set_feed_categories($fid,$categories){
   aggregation_clear_feed_categories($fid);
   if (is_array($categories)) {
     foreach ($categories as $cid) {
       if ($cid) db_query('INSERT INTO {aggregator_category_feed} (fid, cid) VALUES (%d, %d)', $fid, $cid);
     }
   }
 }

 /**
  * Remove all the categories assigned to a feed
  * 
  * @param $fid Feed Id
  */
 function aggregation_clear_feed_categories($fid){
   db_query('DELETE FROM {aggregator_category_feed} WHERE fid=%d', $fid);
 }
 
 
/**
 * Retrieve a list of categories assigned to a feed
 *
 * @param $fid 
 * @return 
 *   An array of category IDs
 */
 function aggregation_get_assigned_feed_categories($fid){
   $result=db_query('SELECT cid FROM {aggregator_category_feed} WHERE fid = %d', $fid);
   while($category=db_fetch_array($result)) {
     $values[]=$category[cid];
   }
   return $values;
 }


/**
 * Load all available feed categories
 * 
 * @return A list of options, Id as key, title as value 
 */
 function aggregation_get_all_feed_categories(){
   static $options;
   if (!$options) {
     // get aggregator categories
     $result = db_query('SELECT title, cid FROM {aggregator_category} ORDER BY title');
     while ($category = db_fetch_array($result)) {
      	$options[$category['cid']]=$category['title'];
     }
   }
  	return $options;
 }




 //=================================== Private Functions =================================//




/**
 * Insert a new feed. It is advised to use the sync_feed function rather than this function 
 * directly so as to make sure that no feeds are duplicated. 
 *
 * @param $feed An array of feed data
 * @return 
 *   A feed id for the inserted feed
 */
 function _aggregation_insert_feed($feed){
   $fid = db_next_id('{aggregator_feed}_fid');
   $feed['refresh'] or $feed['refresh']=3600;
   $feed['block_id'] or $feed['block_id']=5;
   db_query("INSERT INTO {aggregator_feed} (fid, title, url, refresh, block) VALUES (%d, '%s', '%s', %d, %d)",
   $fid,
   $feed['title'],
   $feed['url'],
   $feed['refresh'],
   $feed['block_id']);
   
   if (!db_error()) {
     drupal_set_message(t('A new aggregator feed "%title" was created', array('%title' => $feed['title'])));
   }
   else {
     drupal_set_message(t('A database error occured, no aggregator feed could be created.'), "error");
   }
   return $fid;
 }

/**
 * Update an existing feed. Do not call directly, use sync_feed
 *
 * @param array $feed An array containing the details of the feed to update
 * @return 
 *   A feed id for the updated feed
 */
 function _aggregation_update_feed($feed){
   $feed['refresh'] or $feed['refresh']=3600;
   db_query("UPDATE {aggregator_feed} SET title = '%s', url = '%s', refresh = %d WHERE fid = %d",
   $feed['title'],
   $feed['url'],
   $feed['refresh'],
   $feed['fid']);
   return $feed['fid'];
 }
?>